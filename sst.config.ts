import { Tags } from 'aws-cdk-lib';
import { SSTConfig } from 'sst';
import { API } from './stacks/FplStack';

export default {
    config() {
        return {
            name: 'fpl-tool',
            region: 'eu-west-2',
        };
    },
    stacks(app) {
        Tags.of(app).add('application', 'fpl-tool');
        Tags.of(app).add('created-by', 'sst');

        app.stack(API);

        if (app.stage !== 'production') {
            app.setDefaultRemovalPolicy('destroy');
        }
    },
} satisfies SSTConfig;
