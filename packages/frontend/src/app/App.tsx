import { Container, Grid, Typography } from '@mui/material';
import { useEffect } from 'react';
import AxisSelector from '../components/axis-selector';
import FetchQuerySelector from '../components/fetch-query-selector';
import FilterSelector from '../components/filter-selector';
import HeaderBar from '../components/header-bar';
import PlayerDataPlot from '../components/player-data-plot';
import FetchStatus from '../features/plot/fetch-status';
import {
  fetchHistory,
  fetchStatus,
  fetchTeams,
  updateFilteredPlayers,
} from '../features/plot/plot-slice';
import { useAppDispatch, useAppSelector } from './store';

function App() {
  const dispatch = useAppDispatch();
  const statusFetchStatus = useAppSelector((state) => state.plot.status.fetchStatus);
  const teamsFetchStatus = useAppSelector((state) => state.plot.teams.fetchStatus);
  const playersFetchStatus = useAppSelector((state) => state.plot.players.fetchStatus);

  useEffect(() => {
    dispatch(fetchStatus());
  }, []);

  useEffect(() => {
    if (statusFetchStatus === FetchStatus.Success) {
      dispatch(fetchTeams());
    }
  }, [statusFetchStatus]);

  useEffect(() => {
    if (teamsFetchStatus === FetchStatus.Success) {
      dispatch(fetchHistory());
    }
  }, [teamsFetchStatus]);

  useEffect(() => {
    if (playersFetchStatus === FetchStatus.Success) {
      dispatch(updateFilteredPlayers());
    }
  }, [playersFetchStatus]);

  return (
    <Container maxWidth="xl">
      <HeaderBar />
      {teamsFetchStatus === FetchStatus.Success && (
        <Grid container spacing={0}>
          <Grid item lg={9} sm={12}>
            {playersFetchStatus === FetchStatus.Success && <PlayerDataPlot />}
            {playersFetchStatus === FetchStatus.Fail && (
              <Typography variant="h6" color="error">
                Failed to pull player data, please refresh the page to try again
              </Typography>
            )}
          </Grid>
          <Grid item lg={3} sm={12}>
            <FetchQuerySelector />
            <AxisSelector />
            <FilterSelector />
          </Grid>
        </Grid>
      )}
      {teamsFetchStatus === FetchStatus.Fail && (
        <Container sx={{ display: 'flex', justifyContent: 'center' }}>
          <Typography variant="h6" color="error">
            Failed to pull team data, please refresh the page to try again
          </Typography>
        </Container>
      )}
    </Container>
  );
}

export default App;
