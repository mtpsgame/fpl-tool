import {
  Box,
  Button,
  Checkbox,
  Container,
  FormControl,
  FormControlLabel,
  FormGroup,
  FormLabel,
  TextField,
} from '@mui/material';
import { useCallback } from 'react';
import { useAppDispatch, useAppSelector } from '../../app/store';
import {
  setFilterHomeAway,
  setFilterMaxCost,
  setFilterPosition,
  setFilterTeams,
  setFilterTeamsDeselectAll,
  setFilterTeamsSelectAll,
  updateFilteredPlayers,
} from '../../features/plot/plot-slice';

function FilterSelector() {
  const dispatch = useAppDispatch();
  const teams = useAppSelector((state) => state.plot.teams);
  const filter = useAppSelector((state) => state.plot.filter);

  const setMaxCost = useCallback(
    (cost: number) => {
      dispatch(setFilterMaxCost(cost));
      dispatch(updateFilteredPlayers());
    },
    [dispatch],
  );

  const setHomeAway = useCallback(
    (control: 'home' | 'away', value: boolean) => {
      dispatch(setFilterHomeAway({ control, value }));
      dispatch(updateFilteredPlayers());
    },
    [dispatch],
  );

  const setPosition = useCallback(
    (control: 'gkp' | 'def' | 'mid' | 'fwd', value: boolean) => {
      dispatch(setFilterPosition({ control, value }));
      dispatch(updateFilteredPlayers());
    },
    [dispatch],
  );

  const setTeam = useCallback(
    (control: number, value: boolean) => {
      dispatch(setFilterTeams({ control, value }));
      dispatch(updateFilteredPlayers());
    },
    [dispatch],
  );

  const selectAllTeams = useCallback(() => {
    dispatch(setFilterTeamsSelectAll());
    dispatch(updateFilteredPlayers());
  }, [dispatch]);

  const deselectAllTeams = useCallback(() => {
    dispatch(setFilterTeamsDeselectAll());
    dispatch(updateFilteredPlayers());
  }, [dispatch]);

  return (
    <>
      <Container sx={{ marginTop: '2rem' }}>
        <TextField
          sx={{ maxWidth: '6rem' }}
          type="number"
          label="Max price"
          variant="outlined"
          value={filter.maxCost}
          onChange={(e) => setMaxCost(+e.target.value)}
        />
      </Container>
      <Container sx={{ marginTop: '1rem' }}>
        <Box sx={{ display: 'flex', gap: '3rem' }}>
          <FormControl variant="standard">
            <FormLabel>Venue</FormLabel>
            <FormGroup>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={filter.home}
                    onChange={(_, checked) => setHomeAway('home', checked)}
                    name="home"
                  />
                }
                label="Home"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={filter.away}
                    onChange={(_, checked) => setHomeAway('away', checked)}
                    name="away"
                  />
                }
                label="Away"
              />
            </FormGroup>
          </FormControl>
          <FormControl variant="standard">
            <FormLabel>Position</FormLabel>
            <FormGroup>
              <FormControlLabel
                control={
                  <Checkbox
                    checked={filter.positions.gkp}
                    onChange={(_, checked) => setPosition('gkp', checked)}
                    name="gkp"
                  />
                }
                label="Goalkeeper"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={filter.positions.def}
                    onChange={(_, checked) => setPosition('def', checked)}
                    name="def"
                  />
                }
                label="Defender"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={filter.positions.mid}
                    onChange={(_, checked) => setPosition('mid', checked)}
                    name="mid"
                  />
                }
                label="Midfielder"
              />
              <FormControlLabel
                control={
                  <Checkbox
                    checked={filter.positions.fwd}
                    onChange={(_, checked) => setPosition('fwd', checked)}
                    name="fwd"
                  />
                }
                label="Forward"
              />
            </FormGroup>
          </FormControl>
        </Box>
      </Container>
      <Container sx={{ marginTop: '1rem' }}>
        <Box>
          <FormControl variant="standard">
            <FormLabel>Teams</FormLabel>
            <Box sx={{ display: 'flex', gap: '0.5rem' }}>
              {[...Array(4).keys()].map((col) => (
                <FormGroup>
                  {teams.data
                    ?.filter((_, i) => i >= col * 5 && i < (col + 1) * 5)
                    .map((team) => (
                      <FormControlLabel
                        key={team.id}
                        control={
                          <Checkbox
                            checked={filter.teams[Number(team.id)]}
                            onChange={(_, checked) => setTeam(Number(team.id), checked)}
                            name="home"
                          />
                        }
                        label={team.shortName}
                      />
                    ))}
                </FormGroup>
              ))}
            </Box>
            <Box justifyContent="space-evenly" display="flex">
              <Button
                size="small"
                variant="outlined"
                aria-label="Select all"
                onClick={() => selectAllTeams()}
              >
                Select all
              </Button>
              <Button
                size="small"
                variant="outlined"
                aria-label="Select all"
                onClick={() => deselectAllTeams()}
              >
                Deselect all
              </Button>
            </Box>
          </FormControl>
        </Box>
      </Container>
    </>
  );
}

export default FilterSelector;
