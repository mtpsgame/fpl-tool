import { Box, Tooltip, Typography } from '@mui/material';
import { useEffect, useState } from 'react';
import { useAppSelector } from '../../app/store';
import FetchStatus from '../../features/plot/fetch-status';
import DonationButton from '../donation-button';

function HeaderBar() {
  const fplToolStatus = useAppSelector((state) => state.plot.status);

  const [statusColour, setStatusColour] = useState<'red' | 'green'>('red');
  const [statusText, setStatusText] = useState<'Up to date' | 'Out of date'>('Out of date');

  useEffect(() => {
    if (
      fplToolStatus.data &&
      (Date.now() - Date.parse(fplToolStatus.data.lastUpdated)) / (1000 * 60 * 60 * 24) < 1
    ) {
      setStatusColour('green');
      setStatusText('Up to date');
    } else {
      setStatusColour('red');
      setStatusText('Out of date');
    }
  }, [fplToolStatus]);

  return (
    <Box display="flex" justifyContent="space-between">
      <Box display="inline-block">
        <Typography
          variant="h3"
          display="inline-block"
          sx={{ display: 'inline-block', marginRight: '1rem' }}
        >
          FPL Tool
        </Typography>
        {fplToolStatus.fetchStatus === FetchStatus.Success && (
          <Box display="inline-block">
            <Box
              display="inline-block"
              sx={{
                bgcolor: statusColour,
                width: 10,
                height: 10,
                borderRadius: '50%',
                marginRight: '0.25rem',
              }}
            />
            <Tooltip arrow title={`Last updated at ${fplToolStatus.data?.lastUpdated}`}>
              <Typography display="inline-block" variant="subtitle2">
                {statusText}
              </Typography>
            </Tooltip>
          </Box>
        )}
      </Box>
      <Box display="inline-block">
        <DonationButton />
      </Box>
    </Box>
  );
}

export default HeaderBar;
