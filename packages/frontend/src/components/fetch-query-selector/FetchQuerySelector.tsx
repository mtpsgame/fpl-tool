/* eslint-disable @typescript-eslint/ban-types */
import { HelpOutline } from '@mui/icons-material';
import { Box, Container, Slider, Tooltip, Typography } from '@mui/material';
import debounce from 'lodash.debounce';
import { useCallback, useState } from 'react';
import { AppDispatch, store, useAppDispatch, useAppSelector } from '../../app/store';
import {
  fetchHistory,
  GameweekWindow,
  setMinsPer90,
  setGameweekWindow,
} from '../../features/plot/plot-slice';

function FetchQuerySelector() {
  const dispatch = useAppDispatch();

  const [controlledGameweekWindow, setControlledGameweekWindow] = useState<GameweekWindow>(
    store.getState().plot.query.gameweekWindow,
  );
  const [controlledMinsPer90, setControlledMinsPer90] = useState<number>(
    store.getState().plot.query.minsPer90,
  );
  const queryParameters = useAppSelector((state) => state.plot.query);
  const currentGameweek = useAppSelector((state) => state.plot.status.data?.currentGameweek);

  const debouncedFetch = debounce(
    (action: Function, newValue: number | GameweekWindow, dispatch: AppDispatch) => {
      dispatch(action(newValue));
      dispatch(fetchHistory());
    },
    500,
    {
      leading: true,
    },
  );

  const onChangeCommitted = useCallback(
    (action: Function, oldValue: number, newValue: number | number[]) => {
      newValue = Array.isArray(newValue) ? newValue.pop()! : newValue;
      if (oldValue !== newValue) {
        debouncedFetch(action, newValue, dispatch);
      }
    },
    [debouncedFetch, dispatch],
  );

  const onChange = useCallback(
    (action: Function, value: number | number[]) =>
      action(Array.isArray(value) ? value.pop() : value),
    [],
  );

  const onGameweekWindowChange = useCallback((value: number[]) => {
    setControlledGameweekWindow({
      from: value[0],
      to: value[1],
    });
  }, []);

  const onGameweekWindowChangeCommitted = useCallback(
    (newValue: number[], oldValue: GameweekWindow) => {
      if (newValue[0] !== oldValue.from || newValue[1] !== oldValue.to) {
        debouncedFetch(setGameweekWindow, { from: newValue[0], to: newValue[1] }, dispatch);
      }
    },
    [debouncedFetch, dispatch],
  );

  return (
    <Container>
      <Box display="flex" alignItems="center" gap="0.5rem">
        <Typography display="inline-block" variant="subtitle1">
          Gameweek window
        </Typography>
        <Tooltip
          arrow
          title="Used to select which gameweeks to pull stats from. Selecting 3 - 6 will aggregate the stats from gameweeks 3, 4, 5 and 6."
        >
          <HelpOutline />
        </Tooltip>
      </Box>
      <Slider
        size="medium"
        aria-label="Game window size"
        value={[controlledGameweekWindow.from, controlledGameweekWindow.to]}
        valueLabelDisplay="auto"
        step={1}
        marks
        min={1}
        max={currentGameweek ? parseInt(currentGameweek) : 38}
        onChange={(_, value) => onGameweekWindowChange(value as number[])}
        onChangeCommitted={(_, value) =>
          onGameweekWindowChangeCommitted(value as number[], queryParameters.gameweekWindow)
        }
      />
      <Box display="flex" alignItems="center" gap="0.5rem">
        <Typography display="inline-block" variant="subtitle1">
          Average minutes played per game
        </Typography>
        <Tooltip
          arrow
          title="Only players that have played at least this amount of minutes per game on average within the selected gameweek window are displayed."
        >
          <HelpOutline />
        </Tooltip>
      </Box>
      <Slider
        size="medium"
        aria-label="Minimum average minutes played"
        value={controlledMinsPer90}
        valueLabelDisplay="auto"
        step={1}
        marks
        min={1}
        max={90}
        onChange={(_, value) => onChange(setControlledMinsPer90, value)}
        onChangeCommitted={(_, value) =>
          onChangeCommitted(setMinsPer90, queryParameters.minsPer90, value)
        }
      />
    </Container>
  );
}

export default FetchQuerySelector;
