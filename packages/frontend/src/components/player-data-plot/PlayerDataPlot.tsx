import { useTheme } from '@mui/material';
import Plot from 'react-plotly.js';
import { useAppSelector } from '../../app/store';
import { axes } from '../../features/plot/plot-slice';

function PlayerDataPlot() {
  const theme = useTheme();
  const players = useAppSelector((state) => state.plot.players.filtered);
  const axisKeys = useAppSelector((state) => state.plot.axisKeys);
  return (
    <Plot
      data={[
        {
          x: players?.map(axes[axisKeys.x].getValue),
          y: players?.map(axes[axisKeys.y].getValue),
          type: 'scatter',
          mode: 'markers',
          marker: { color: 'red' },
          text: players?.map((p) => p.name),
        },
      ]}
      useResizeHandler
      layout={{
        autosize: true,
        font: {
          family: theme.typography.fontFamily,
        },
        xaxis: {
          title: axes[axisKeys.x].displayName,
        },
        yaxis: {
          title: axes[axisKeys.y].displayName,
        },
      }}
      config={{
        scrollZoom: true,
        modeBarButtonsToRemove: ['select2d', 'lasso2d', 'autoScale2d'],
      }}
      style={{
        width: '100%',
        height: '75vh',
        minHeight: 500,
      }}
    />
  );
}

export default PlayerDataPlot;
