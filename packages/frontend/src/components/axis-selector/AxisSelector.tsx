import {
  Container,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material';
import { useCallback } from 'react';
import { useAppDispatch, useAppSelector } from '../../app/store';
import { axes, setAxis } from '../../features/plot/plot-slice';

const axisMenuItems = () =>
  Object.keys(axes).map((axisKey) => (
    <MenuItem key={axisKey} value={axisKey}>
      {axes[axisKey].displayName}
    </MenuItem>
  ));

function AxisSelector() {
  const dispatch = useAppDispatch();
  const axisKeys = useAppSelector((state) => state.plot.axisKeys);

  const onAxisChange = useCallback(
    (axis: 'x' | 'y', axisKey: string) => dispatch(setAxis({ axis, axisKey })),
    [dispatch],
  );

  return (
    <Container sx={{ display: 'flex', gap: '1rem', marginTop: '2rem' }}>
      <FormControl>
        <InputLabel id="x-axis">X axis</InputLabel>
        <Select
          sx={{ minWidth: '9rem' }}
          id="x-axis"
          labelId="x-axis"
          label="X Axis"
          value={axisKeys.x}
          onChange={(event: SelectChangeEvent) => onAxisChange('x', event.target.value)}
        >
          {axisMenuItems()}
        </Select>
      </FormControl>
      <FormControl>
        <InputLabel id="y-axis">Y axis</InputLabel>
        <Select
          sx={{ minWidth: '9rem' }}
          id="y-axis"
          labelId="y-axis"
          label="Y Axis"
          value={axisKeys.y}
          onChange={(event: SelectChangeEvent) => onAxisChange('y', event.target.value)}
        >
          {axisMenuItems()}
        </Select>
      </FormControl>
    </Container>
  );
}

export default AxisSelector;
