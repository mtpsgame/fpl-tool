function DonationButton() {
  return (
    <a href="https://www.buymeacoffee.com/mtpsgame" target="_blank" rel="noreferrer">
      <img
        src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png"
        alt="Buy Me A Coffee"
        style={{ height: '2.5rem' }}
      />
    </a>
  );
}

export default DonationButton;
