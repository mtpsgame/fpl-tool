import axios from 'axios';
import { PlayerHistory } from '../../../core/src/models/player-history.model';
import { Status } from '../../../core/src/models/status.model';
import { Team } from '../../../core/src/models/team.model';

const client = axios.create({
  baseURL: `${import.meta.env.VITE_API_BASE_URL}`,
});

const DataService = {
  async fetchStatus(): Promise<Status | undefined> {
    return (await client.get('status')).data;
  },
  async fetchTeams(): Promise<Team[]> {
    return (await client.get('teams')).data;
  },
  async fetchHistory(
    fromGameweek: number,
    toGameweek: number,
    minsPer90: number,
  ): Promise<PlayerHistory[]> {
    return (
      await client.get(`history`, {
        params: {
          fromGameweek: fromGameweek,
          toGameweek: toGameweek,
          minimumAvgMinsPer90: minsPer90,
        },
      })
    ).data;
  },
};

export default DataService;
