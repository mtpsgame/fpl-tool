import { GameStats } from '../../../../core/src/models/player-game-stats.model';
import { PlayerHistory } from '../../../../core/src/models/player-history.model';
import { PlotFilterState } from './plot-slice';

export function playerFilter(player: PlayerHistory, filter: PlotFilterState) {
  if (player.cost > filter.maxCost) {
    return false;
  }
  switch (player.position) {
    case 1:
      if (!filter.positions.gkp) {
        return false;
      }
      break;
    case 2:
      if (!filter.positions.def) {
        return false;
      }
      break;
    case 3:
      if (!filter.positions.mid) {
        return false;
      }
      break;
    case 4:
      if (!filter.positions.fwd) {
        return false;
      }
      break;
    default:
      return false;
  }
  return true;
}

export function gameFilter(game: GameStats, filter: PlotFilterState) {
  return (game.wasHome && filter.home) || (!game.wasHome && filter.away);
}

export function teamFilter(player: PlayerHistory, filter: PlotFilterState) {
  return filter.teams[Number(player.teamId)];
}
