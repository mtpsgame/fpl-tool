import { PayloadAction, createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { PlayerHistory } from '../../../../core/src/models/player-history.model';
import { Status } from '../../../../core/src/models/status.model';
import { Team } from '../../../../core/src/models/team.model';
import { RootState } from '../../app/store';
import DataService from '../../services/dataService';
import FetchStatus from './fetch-status';
import { gameFilter, playerFilter, teamFilter } from './plot-filter';

interface PlotState {
  status: {
    data: Status | undefined;
    fetchStatus: FetchStatus;
  };
  teams: {
    data: Team[] | undefined;
    fetchStatus: FetchStatus;
  };
  players: {
    all: PlayerHistory[] | undefined;
    filtered: PlayerHistory[] | undefined;
    fetchStatus: FetchStatus;
  };
  query: {
    gameweekWindow: GameweekWindow;
    minsPer90: number;
  };
  axisKeys: {
    x: string;
    y: string;
  };
  filter: PlotFilterState;
}

type GameweekWindow = {
  from: number;
  to: number;
};

interface PlotFilterState {
  maxCost: number;
  home: boolean;
  away: boolean;
  positions: {
    gkp: boolean;
    def: boolean;
    mid: boolean;
    fwd: boolean;
  };
  teams: Record<number, boolean>;
}

const axes: {
  [key: string]: {
    displayName: string;
    getValue: (player: PlayerHistory) => number;
  };
} = {
  minutes: {
    displayName: 'Minutes',
    getValue: (player) =>
      player.stats.map((game) => game.minutes).reduce((sum, next) => sum + next, 0),
  },
  goals: {
    displayName: 'Goals',
    getValue: (player) =>
      player.stats.map((game) => game.goals).reduce((sum, next) => sum + next, 0),
  },
  xg: {
    displayName: 'xGoals',
    getValue: (player) => player.stats.map((game) => game.xG).reduce((sum, next) => sum + next, 0),
  },
  assists: {
    displayName: 'Assists',
    getValue: (player) =>
      player.stats.map((game) => game.assists).reduce((sum, next) => sum + next, 0),
  },
  xa: {
    displayName: 'xAssists',
    getValue: (player) => player.stats.map((game) => game.xA).reduce((sum, next) => sum + next, 0),
  },
  involvements: {
    displayName: 'Involvements',
    getValue: (player) =>
      player.stats.map((game) => game.goals + game.assists).reduce((sum, next) => sum + next, 0),
  },
  xInvolvements: {
    displayName: 'xInvolvements',
    getValue: (player) =>
      player.stats.map((game) => game.xG + game.xA).reduce((sum, next) => sum + next, 0),
  },
  conceded: {
    displayName: 'Conceded',
    getValue: (player) =>
      player.stats.map((game) => game.conceded).reduce((sum, next) => sum + next, 0),
  },
  xc: {
    displayName: 'xConceded',
    getValue: (player) => player.stats.map((game) => game.xC).reduce((sum, next) => sum + next, 0),
  },
  saves: {
    displayName: 'Saves',
    getValue: (player) =>
      player.stats.map((game) => game.saves).reduce((sum, next) => sum + next, 0),
  },
  bps: {
    displayName: 'BPS',
    getValue: (player) => player.stats.map((game) => game.bps).reduce((sum, next) => sum + next, 0),
  },
  points: {
    displayName: 'Points',
    getValue: (player) =>
      player.stats.map((game) => game.totalPoints).reduce((sum, next) => sum + next, 0),
  },
  bonusPoints: {
    displayName: 'Bonus Points',
    getValue: (player) => player.stats.map((game) => game.bps).reduce((sum, next) => sum + next, 0),
  },
  cost: {
    displayName: 'Cost',
    getValue: (player) => player.cost,
  },
};

const initialState: PlotState = {
  status: {
    data: undefined,
    fetchStatus: FetchStatus.Init,
  },
  teams: {
    data: undefined,
    fetchStatus: FetchStatus.Init,
  },
  players: {
    all: undefined,
    filtered: undefined,
    fetchStatus: FetchStatus.Init,
  },
  query: {
    gameweekWindow: {
      from: 1,
      to: 5,
    },
    minsPer90: 60,
  },
  axisKeys: {
    x: 'involvements',
    y: 'xInvolvements',
  },
  filter: {
    maxCost: 20,
    home: true,
    away: true,
    positions: {
      gkp: true,
      def: true,
      mid: true,
      fwd: true,
    },
    teams: {
      '1': true,
      '2': true,
      '3': true,
      '4': true,
      '5': true,
      '6': true,
      '7': true,
      '8': true,
      '9': true,
      '10': true,
      '11': true,
      '12': true,
      '13': true,
      '14': true,
      '15': true,
      '16': true,
      '17': true,
      '18': true,
      '19': true,
      '20': true,
    },
  },
};

const fetchStatus = createAsyncThunk<Status | undefined, void, { state: RootState }>(
  'plot/fetchStatus',
  async () => await DataService.fetchStatus(),
);

const fetchTeams = createAsyncThunk<Team[], void, { state: RootState }>(
  'plot/fetchTeams',
  async () => await DataService.fetchTeams(),
);

const fetchHistory = createAsyncThunk<PlayerHistory[], void, { state: RootState }>(
  'plot/fetchHistory',
  async (_, thunkAPI) => {
    const { gameweekWindow, minsPer90 } = thunkAPI.getState().plot.query;
    return await DataService.fetchHistory(gameweekWindow.from, gameweekWindow.to, minsPer90);
  },
);

const plotSlice = createSlice({
  name: 'plot',
  initialState,
  reducers: {
    setGameweekWindow(state, action: PayloadAction<GameweekWindow>) {
      state.query.gameweekWindow = action.payload;
    },
    setMinsPer90(state, action: PayloadAction<number>) {
      state.query.minsPer90 = action.payload;
    },
    setAxis(state, action: PayloadAction<{ axis: 'x' | 'y'; axisKey: string }>) {
      state.axisKeys[action.payload.axis] = action.payload.axisKey;
    },
    updateFilteredPlayers(state) {
      if (state.players.all === undefined) {
        return;
      }
      const filteredPlayers = state.players.all
        .filter((player) => teamFilter(player, state.filter))
        .filter((player) => playerFilter(player, state.filter));
      state.players.filtered = filteredPlayers.map((player) => ({
        ...player,
        stats: player.stats.filter((game) => gameFilter(game, state.filter)),
      }));
    },
    setFilterMaxCost(state, action: PayloadAction<number>) {
      state.filter.maxCost = action.payload;
    },
    setFilterHomeAway(state, action: PayloadAction<{ control: 'home' | 'away'; value: boolean }>) {
      state.filter[action.payload.control] = action.payload.value;
    },
    setFilterPosition(
      state,
      action: PayloadAction<{ control: 'gkp' | 'def' | 'mid' | 'fwd'; value: boolean }>,
    ) {
      state.filter.positions[action.payload.control] = action.payload.value;
    },
    setFilterTeams(
      state,
      action: PayloadAction<{
        control: number;
        value: boolean;
      }>,
    ) {
      state.filter.teams[action.payload.control] = action.payload.value;
    },
    setFilterTeamsSelectAll(state) {
      for (let i = 1; i <= 20; i++) {
        state.filter.teams[i] = true;
      }
    },
    setFilterTeamsDeselectAll(state) {
      for (let i = 1; i <= 20; i++) {
        state.filter.teams[i] = false;
      }
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchStatus.pending, (state) => {
      state.status.fetchStatus = FetchStatus.Init;
    });
    builder.addCase(fetchStatus.fulfilled, (state, action) => {
      state.status.data = action.payload;
      if (action.payload?.currentGameweek) {
        const currentGameweek = parseInt(action.payload.currentGameweek);
        state.query.gameweekWindow.to = currentGameweek;
        state.query.gameweekWindow.from = Math.max(1, currentGameweek - 5);
      }
      state.status.fetchStatus = FetchStatus.Success;
    });
    builder.addCase(fetchStatus.rejected, (state) => {
      state.status.data = undefined;
      state.status.fetchStatus = FetchStatus.Fail;
    });
    builder.addCase(fetchTeams.pending, (state) => {
      state.teams.fetchStatus = FetchStatus.InProgress;
    });
    builder.addCase(fetchTeams.fulfilled, (state, action) => {
      state.teams.data = action.payload;
      state.teams.fetchStatus = FetchStatus.Success;
    });
    builder.addCase(fetchTeams.rejected, (state) => {
      state.teams.data = undefined;
      state.teams.fetchStatus = FetchStatus.Fail;
    });
    builder.addCase(fetchHistory.pending, (state) => {
      state.players.fetchStatus = FetchStatus.InProgress;
    });
    builder.addCase(fetchHistory.fulfilled, (state, action) => {
      state.players.all = action.payload;
      state.players.fetchStatus = FetchStatus.Success;
    });
    builder.addCase(fetchHistory.rejected, (state) => {
      state.players.all = undefined;
      state.players.filtered = undefined;
      state.players.fetchStatus = FetchStatus.Fail;
    });
  },
});

export { axes, fetchHistory, fetchStatus, fetchTeams, plotSlice };
export type { PlotFilterState, PlotState, GameweekWindow };
export const {
  setAxis,
  setGameweekWindow,
  setMinsPer90,
  updateFilteredPlayers,
  setFilterMaxCost,
  setFilterHomeAway,
  setFilterPosition,
  setFilterTeams,
  setFilterTeamsSelectAll,
  setFilterTeamsDeselectAll,
} = plotSlice.actions;
export default plotSlice.reducer;
