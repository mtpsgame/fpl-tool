enum Position {
  GK = 1,
  DEF = 2,
  MID = 3,
  FWD = 4,
}

export default Position;
