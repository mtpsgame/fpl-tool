enum FetchStatus {
  Init,
  InProgress,
  Success,
  Fail,
}

export default FetchStatus;
