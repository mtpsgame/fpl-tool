import 'reflect-metadata';
import { DailyUpdateHandler } from '@fpl-tool/core/handlers/daily-update.handler';
import { resolve } from '../ioc-container';

export async function handle(): Promise<void> {
    const handler: DailyUpdateHandler = resolve(DailyUpdateHandler);
    await handler.handle();
}
