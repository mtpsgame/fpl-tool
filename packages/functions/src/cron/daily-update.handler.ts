import 'reflect-metadata';
import { resolve } from '../ioc-container';
import { IQueueSender } from '@fpl-tool/core/ports/services/queue-sender.port';

export async function handle(): Promise<void> {
    const handler: IQueueSender = resolve('IQueueSender');
    await handler.send('daily-update-queue', 'triggerUpdate');
}
