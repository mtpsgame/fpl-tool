import 'reflect-metadata';
import { GetHistoryHandler } from '@fpl-tool/core/handlers/get-history.handler';
import { GetStatusHandler } from '@fpl-tool/core/handlers/get-status.handler';
import { GetTeamsHandler } from '@fpl-tool/core/handlers/get-teams.handler';
import { ApiHandler, useQueryParam } from 'sst/node/api';
import { resolve } from '../ioc-container';

export const getStatusHandler = ApiHandler(async () => {
    const handler: GetStatusHandler = resolve(GetStatusHandler);
    const status = await handler.handle();

    return {
        statusCode: 200,
        body: JSON.stringify(status),
    };
});

export const getTeamsHandler = ApiHandler(async () => {
    const handler: GetTeamsHandler = resolve(GetTeamsHandler);
    const teams = await handler.handle();

    return {
        statusCode: 200,
        body: JSON.stringify(teams),
    };
});

export const getHistoryHandler = ApiHandler(async () => {
    const fromGameweek = Number(useQueryParam('fromGameweek'));
    const toGameweek = Number(useQueryParam('toGameweek'));
    const minimumAvgMinsPer90 = Number(useQueryParam('minimumAvgMinsPer90'));

    if (!fromGameweek || !toGameweek || !minimumAvgMinsPer90) {
        return {
            statusCode: 400,
            body: 'Bad request. Must include integer `fromGameweek`, `toGameweek` and `minimumAvgMinsPer90` query parameters.',
        };
    }

    if (fromGameweek < 1 || fromGameweek > 38) {
        return {
            statusCode: 400,
            body: 'Bad request. `fromGameweek` must be between 1 and 38 inclusive.',
        };
    }

    if (toGameweek < 1 || toGameweek > 38) {
        return {
            statusCode: 400,
            body: 'Bad request. `toGameweek` must be between 1 and 38 inclusive.',
        };
    }

    if (fromGameweek > toGameweek) {
        return {
            statusCode: 400,
            body: 'Bad request. `fromGameweek` must be less than or equal to `toGameweek`.',
        };
    }

    if (minimumAvgMinsPer90 < 0 || minimumAvgMinsPer90 > 90) {
        return {
            statusCode: 400,
            body: 'Bad request. `minimumAvgMinsPer90` must be between 0 and 90 inclusive.',
        };
    }

    const handler: GetHistoryHandler = resolve(GetHistoryHandler);
    const response = await handler.handle(fromGameweek, toGameweek, minimumAvgMinsPer90);

    return {
        statusCode: 200,
        body: JSON.stringify(response),
    };
});
