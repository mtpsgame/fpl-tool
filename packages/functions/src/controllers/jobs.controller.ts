import { IQueueSender } from '@fpl-tool/core/ports/services/queue-sender.port';
import 'reflect-metadata';
import { resolve } from 'src/ioc-container';
import { ApiHandler } from 'sst/node/api';

export const updateHandler = ApiHandler(async () => {
    const handler: IQueueSender = resolve('IQueueSender');
    await handler.send('daily-update-queue', 'triggerUpdate');

    return {
        statusCode: 200,
        body: 'Update triggered',
    };
});
