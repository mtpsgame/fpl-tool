import { FixtureRepository } from '@fpl-tool/core/repositories/fixture.repository';
import { GameweekPlayerStatsRepository } from '@fpl-tool/core/repositories/gameweek-player-stats.repository';
import { PlayerGameStatsRepository } from '@fpl-tool/core/repositories/player-game-stats.repository';
import { PlayerRepository } from '@fpl-tool/core/repositories/player.repository';
import { StatusRepository } from '@fpl-tool/core/repositories/status.repository';
import { TeamRepository } from '@fpl-tool/core/repositories/team.repository';
import { FplDataService } from '@fpl-tool/core/services/fpl-data.service';
import { QueueSender } from '@fpl-tool/core/services/queue-sender.service';
import { InjectionToken, container } from 'tsyringe';

container.register('IQueueSender', { useClass: QueueSender });
container.register('IStatusRepository', { useClass: StatusRepository });
container.register('ITeamRepository', { useClass: TeamRepository });
container.register('IPlayerRepository', { useClass: PlayerRepository });
container.register('IFixtureRepository', { useClass: FixtureRepository });
container.register('IPlayerGameStatsRepository', { useClass: PlayerGameStatsRepository });
container.register('IGameweekPlayerStatsRepository', { useClass: GameweekPlayerStatsRepository });
container.register('IFplDataService', { useClass: FplDataService });

export function resolve(token: InjectionToken) {
    return container.resolve(token);
}
