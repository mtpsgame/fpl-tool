import axios, { AxiosInstance } from 'axios';
import { BootstrapDto, BootstrapDtoSchema } from '../dtos/bootstrap.dto';
import { FixturesDto, FixturesDtoSchema } from '../dtos/fixture.dto';
import { PlayerHistoryDto, PlayerHistoryDtoSchema } from '../dtos/player-history.dto';
import { Fixture, FixtureSchema } from '../models/fixture.model';
import { PlayerGameStats, PlayerGameStatsSchema } from '../models/player-game-stats.model';
import { Player, PlayerSchema } from '../models/player.model';
import { Team, TeamSchema } from '../models/team.model';
import { Bootstrap, IFplDataService } from '../ports/services/fpl-data.service.port';

const fplHttpClient = axios.create({
    baseURL: 'https://fantasy.premierleague.com/api',
});

export class FplDataService implements IFplDataService {
    constructor(private readonly client: AxiosInstance = fplHttpClient) {}

    async getBootstrapData(): Promise<Bootstrap> {
        const response = await this.client.get('/bootstrap-static/');
        const { success, data, error } = BootstrapDtoSchema.safeParse(response.data);
        if (!success) {
            throw new Error('Failed to parse bootstrap response', error);
        }
        return {
            teams: data.teams.map(this.parseTeam).filter((t): t is Team => t !== undefined),
            players: data.elements
                .map(this.parsePlayer)
                .filter((p): p is Player => p !== undefined),
        };
    }

    async getFixtures(): Promise<Fixture[]> {
        const response = await this.client.get('/fixtures/');
        const { success, data, error } = FixturesDtoSchema.safeParse(response.data);
        if (!success) {
            throw new Error('Failed to parse fixtures response', error);
        }
        return data.map(this.parseFixture).filter((f): f is Fixture => f !== undefined);
    }

    async getPlayerHistory(playerId: number): Promise<PlayerGameStats[]> {
        const response = await this.client.get(`/element-summary/${playerId}/`);
        const { success, data, error } = PlayerHistoryDtoSchema.safeParse(response.data);
        if (!success) {
            throw new Error('Failed to parse element summary response', error);
        }
        return data.history
            .map(this.parsePlayerFixtureStats)
            .filter((p): p is PlayerGameStats => p !== undefined);
    }

    private parseTeam(team: BootstrapDto['teams'][0]): Team | undefined {
        const result = TeamSchema.safeParse({
            id: team.id,
            name: team.name,
            shortName: team.short_name,
        });
        if (result.success) {
            return result.data;
        }
        console.error('Error parsing team', team, result.error);
    }

    private parsePlayer(player: BootstrapDto['elements'][0]): Player | undefined {
        const result = PlayerSchema.safeParse({
            id: player.id,
            name: player.web_name,
            teamId: player.team,
            position: player.element_type,
            cost: player.now_cost,
            selectedByPercent: player.selected_by_percent,
            status: player.status,
        });
        if (result.success) {
            return result.data;
        }
        console.error('Error parsing player', player, result.error);
    }

    private parseFixture(fixture: FixturesDto[0]): Fixture | undefined {
        const result = FixtureSchema.safeParse({
            id: fixture.id,
            gameweek: fixture.event,
            kickoffTime: fixture.kickoff_time,
            homeTeamId: fixture.team_h,
            awayTeamId: fixture.team_a,
            homeTeamScore: fixture.team_h_score ?? undefined,
            awayTeamScore: fixture.team_a_score ?? undefined,
        });
        if (result.success) {
            return result.data;
        }
        console.error('Error parsing fixture', fixture, result.error);
    }

    private parsePlayerFixtureStats(
        fixtureStats: PlayerHistoryDto['history'][0],
    ): PlayerGameStats | undefined {
        const result = PlayerGameStatsSchema.safeParse({
            playerId: fixtureStats.element,
            fixture: fixtureStats.fixture,
            gameweek: fixtureStats.round,
            wasHome: fixtureStats.was_home,
            totalPoints: fixtureStats.total_points,
            minutes: fixtureStats.minutes,
            goals: fixtureStats.goals_scored,
            assists: fixtureStats.assists,
            conceded: fixtureStats.goals_conceded,
            xG: fixtureStats.expected_goals,
            xA: fixtureStats.expected_assists,
            xC: fixtureStats.expected_goals_conceded,
            saves: fixtureStats.saves,
            bonus: fixtureStats.bonus,
            bps: fixtureStats.bps,
        });
        if (result.success) {
            return result.data;
        }
        console.error('Error parsing player fixture stats', fixtureStats, result.error);
    }
}
