import { SendMessageCommand, SQSClient } from '@aws-sdk/client-sqs';
import { IQueueSender } from '../ports/services/queue-sender.port';
import { Queue, QueueResources } from 'sst/node/queue';

const sqs = new SQSClient({});

export class QueueSender implements IQueueSender {
    async send(queue: keyof QueueResources, message: string): Promise<void> {
        const command = new SendMessageCommand({
            QueueUrl: Queue[queue].queueUrl,
            MessageBody: message,
        });
        await sqs.send(command);
    }
}
