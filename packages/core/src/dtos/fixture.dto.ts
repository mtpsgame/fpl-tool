import { z } from 'zod';

export const FixturesDtoSchema = z.array(
    z.object({
        id: z.number(),
        event: z.number(),
        kickoff_time: z.string(),
        team_h: z.number(),
        team_a: z.number(),
        team_h_score: z.number().nullable(),
        team_a_score: z.number().nullable(),
    }),
);
export type FixturesDto = z.infer<typeof FixturesDtoSchema>;
