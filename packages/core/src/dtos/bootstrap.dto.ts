import { z } from 'zod';

export const BootstrapDtoSchema = z.object({
    events: z.array(
        z.object({
            id: z.number(),
            name: z.string(),
            deadline_time: z.string(),
            is_current: z.boolean(),
        }),
    ),
    teams: z.array(
        z.object({
            id: z.number(),
            name: z.string(),
            short_name: z.string(),
            position: z.number(),
        }),
    ),
    elements: z.array(
        z.object({
            id: z.number(),
            web_name: z.string(),
            team: z.number(),
            element_type: z.number(),
            now_cost: z.number().transform((val) => val / 10),
            selected_by_percent: z.string().transform((val) => Number(val)),
            status: z.string(),
        }),
    ),
});
export type BootstrapDto = z.infer<typeof BootstrapDtoSchema>;
