import { z } from 'zod';

export const PlayerHistoryDtoSchema = z.object({
    history: z.array(
        z.object({
            element: z.number(),
            fixture: z.number(),
            round: z.number(),
            was_home: z.boolean(),
            total_points: z.number(),
            minutes: z.number(),
            goals_scored: z.number(),
            assists: z.number(),
            goals_conceded: z.number(),
            expected_goals: z.string().transform((val) => Number(val)),
            expected_assists: z.string().transform((val) => Number(val)),
            expected_goals_conceded: z.string().transform((val) => Number(val)),
            saves: z.number(),
            bonus: z.number(),
            bps: z.number(),
        }),
    ),
});
export type PlayerHistoryDto = z.infer<typeof PlayerHistoryDtoSchema>;
