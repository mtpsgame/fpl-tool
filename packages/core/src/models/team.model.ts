import { z } from 'zod';

export const TeamIdSchema = z.coerce
    .number()
    .int()
    .min(1)
    .max(20)
    .transform((id) => String(id).padStart(2, '0'));
export type TeamId = z.infer<typeof TeamIdSchema>;

export const TeamSchema = z.object({
    id: TeamIdSchema,
    shortName: z.string().length(3),
    name: z.string().min(3),
});
export type Team = z.infer<typeof TeamSchema>;
