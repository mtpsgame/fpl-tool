import { z } from 'zod';
import { TeamIdSchema } from './team.model';

export const GameweekIdSchema = z.coerce
    .number()
    .int()
    .min(1)
    .max(38)
    .transform((id) => String(id).padStart(2, '0'));
export type GameweekId = z.infer<typeof GameweekIdSchema>;

export const FixtureIdSchema = z.coerce
    .number()
    .int()
    .positive()
    .transform((id) => String(id).padStart(3, '0'));
export type FixtureId = z.infer<typeof FixtureIdSchema>;

export const FixtureSchema = z.object({
    id: FixtureIdSchema,
    gameweek: GameweekIdSchema,
    kickoffTime: z.string().datetime(),
    homeTeamId: TeamIdSchema,
    awayTeamId: TeamIdSchema,
    homeTeamScore: z.number().int().nonnegative().optional(),
    awayTeamScore: z.number().int().nonnegative().optional(),
});
export type Fixture = z.infer<typeof FixtureSchema>;
