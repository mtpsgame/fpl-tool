import { z } from 'zod';
import { GameStats } from './player-game-stats.model';
import { PlayerSchema } from './player.model';

export const PlayerHistorySchema = PlayerSchema.extend({
    stats: z.array(GameStats),
});
export type PlayerHistory = z.infer<typeof PlayerHistorySchema>;
