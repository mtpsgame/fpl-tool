import { z } from 'zod';
import { GameweekIdSchema } from './fixture.model';
import { PlayerGameStatsSchema } from './player-game-stats.model';
import { PlayerIdSchema } from './player.model';

export const GameweekPlayerStatsSchema = z.object({
    gameweek: GameweekIdSchema,
    stats: z.array(
        z.object({
            playerId: PlayerIdSchema,
            playerStats: z.array(PlayerGameStatsSchema),
        }),
    ),
});
export type GameweekPlayerStats = z.infer<typeof GameweekPlayerStatsSchema>;
