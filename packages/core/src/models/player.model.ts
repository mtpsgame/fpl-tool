import { z } from 'zod';
import { TeamIdSchema } from './team.model';

export const PlayerIdSchema = z.coerce
    .number()
    .int()
    .positive()
    .transform((id) => String(id).padStart(4, '0'));
export type PlayerId = z.infer<typeof PlayerIdSchema>;

export const PlayerSchema = z.object({
    id: PlayerIdSchema,
    name: z.string().min(1),
    teamId: TeamIdSchema,
    position: z.number().int().min(1).max(4),
    cost: z.number().positive(),
    selectedByPercent: z.number().min(0).max(100),
    status: z.string().length(1),
});
export type Player = z.infer<typeof PlayerSchema>;
