import { z } from 'zod';
import { GameweekIdSchema } from './fixture.model';

export const StatusSchema = z.object({
    lastUpdated: z.string().datetime(),
    currentGameweek: GameweekIdSchema,
});
export type Status = z.infer<typeof StatusSchema>;
