import { z } from 'zod';
import { FixtureIdSchema, GameweekIdSchema } from './fixture.model';
import { PlayerIdSchema } from './player.model';

export const PlayerGameStatsSchema = z.object({
    playerId: PlayerIdSchema,
    fixture: FixtureIdSchema,
    gameweek: GameweekIdSchema,
    wasHome: z.boolean(),
    totalPoints: z.number().int(),
    minutes: z.number().int().nonnegative(),
    goals: z.number().int().nonnegative(),
    assists: z.number().int().nonnegative(),
    conceded: z.number().int().nonnegative(),
    xG: z.number().nonnegative(),
    xA: z.number().nonnegative(),
    xC: z.number().nonnegative(),
    saves: z.number().int().nonnegative(),
    bonus: z.number().int().nonnegative(),
    bps: z.number().int(),
});
export type PlayerGameStats = z.infer<typeof PlayerGameStatsSchema>;

export const GameStats = PlayerGameStatsSchema.omit({ playerId: true });
export type GameStats = z.infer<typeof GameStats>;
