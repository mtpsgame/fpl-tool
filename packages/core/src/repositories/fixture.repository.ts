import { Entity } from 'electrodb';
import { Fixture, FixtureSchema } from '../models/fixture.model';
import { IFixtureRepository } from '../ports/repositories/fixture.repository.port';
import { Configuration } from './fpl-dynamo';

const FixtureEntity = new Entity(
    {
        model: {
            entity: 'fixture',
            version: '1',
            service: 'fpl',
        },
        attributes: {
            id: {
                type: 'string',
                required: true,
            },
            gameweek: {
                type: 'string',
                required: true,
            },
            kickoffTime: {
                type: 'string',
                required: true,
            },
            homeTeamId: {
                type: 'string',
                required: true,
            },
            awayTeamId: {
                type: 'string',
                required: true,
            },
            homeTeamScore: {
                type: 'number',
                required: false,
            },
            awayTeamScore: {
                type: 'number',
                required: false,
            },
        },
        indexes: {
            primary: {
                pk: {
                    field: 'pk',
                    composite: [],
                    template: 'fixture',
                },
                sk: {
                    field: 'sk',
                    composite: ['id'],
                    template: '${id}',
                },
            },
        },
    },
    Configuration,
);

export class FixtureRepository implements IFixtureRepository {
    async getAll(): Promise<Fixture[]> {
        const status = await FixtureEntity.query.primary({}).go();
        return status.data.map((fixture) => ({
            id: fixture.id,
            gameweek: fixture.gameweek,
            kickoffTime: fixture.kickoffTime,
            homeTeamId: fixture.homeTeamId,
            awayTeamId: fixture.awayTeamId,
            homeTeamScore: fixture.homeTeamScore,
            awayTeamScore: fixture.awayTeamScore,
        }));
    }

    async update(fixture: Fixture): Promise<void> {
        const { id, gameweek, kickoffTime, homeTeamId, awayTeamId, homeTeamScore, awayTeamScore } =
            FixtureSchema.parse(fixture);
        await FixtureEntity.upsert({
            id: id,
            gameweek: gameweek,
            kickoffTime: kickoffTime,
            homeTeamId: homeTeamId,
            awayTeamId: awayTeamId,
            homeTeamScore: homeTeamScore,
            awayTeamScore: awayTeamScore,
        }).go();
    }
}
