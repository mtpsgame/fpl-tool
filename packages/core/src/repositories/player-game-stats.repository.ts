import { Entity } from 'electrodb';
import { GameweekId, GameweekIdSchema } from '../models/fixture.model';
import { PlayerGameStats, PlayerGameStatsSchema } from '../models/player-game-stats.model';
import { IPlayerGameStatsRepository } from '../ports/repositories/player-game-stats.repository.port';
import { Configuration } from './fpl-dynamo';

const PlayerFixtureStatsEntity = new Entity(
    {
        model: {
            entity: 'player-fixture-stats',
            version: '1',
            service: 'fpl',
        },
        attributes: {
            playerId: {
                type: 'string',
                required: true,
            },
            fixture: {
                type: 'string',
                required: true,
            },
            gameweek: {
                type: 'string',
                required: true,
            },
            wasHome: {
                type: 'boolean',
                required: true,
            },
            totalPoints: {
                type: 'number',
                required: true,
            },
            minutes: {
                type: 'number',
                required: true,
            },
            goals: {
                type: 'number',
                required: true,
            },
            assists: {
                type: 'number',
                required: true,
            },
            conceded: {
                type: 'number',
                required: true,
            },
            xG: {
                type: 'number',
                required: true,
            },
            xA: {
                type: 'number',
                required: true,
            },
            xC: {
                type: 'number',
                required: true,
            },
            saves: {
                type: 'number',
                required: true,
            },
            bonus: {
                type: 'number',
                required: true,
            },
            bps: {
                type: 'number',
                required: true,
            },
        },
        indexes: {
            primary: {
                pk: {
                    field: 'pk',
                    composite: ['playerId'],
                    template: 'player#${playerId}',
                },
                sk: {
                    field: 'sk',
                    composite: ['gameweek', 'fixture'],
                    template: 'gameweek#${gameweek}#fixture#${fixture}',
                },
            },
            gsi1: {
                index: 'gsi1',
                pk: {
                    field: 'gsi1pk',
                    composite: ['gameweek'],
                    template: 'gameweek#${gameweek}',
                },
                sk: {
                    field: 'gsi1sk',
                    composite: ['playerId', 'fixture'],
                    template: 'player#${playerId}#fixture#${fixture}',
                },
            },
        },
    },
    Configuration,
);

export class PlayerGameStatsRepository implements IPlayerGameStatsRepository {
    async getAllInGameweekRange(
        fromGameweek: number,
        toGameweek: number,
    ): Promise<Map<GameweekId, PlayerGameStats[]>> {
        const gameweekMap: Map<GameweekId, PlayerGameStats[]> = new Map();
        for (let gameweek = fromGameweek; gameweek <= toGameweek; gameweek++) {
            const parsedGameweek = GameweekIdSchema.parse(gameweek);
            const result = await PlayerFixtureStatsEntity.query
                .gsi1({ gameweek: parsedGameweek })
                .go();
            if (result.data.length === 0) {
                continue;
            }
            gameweekMap.set(
                parsedGameweek,
                result.data.map((data) => ({
                    playerId: data.playerId,
                    fixture: data.fixture,
                    gameweek: data.gameweek,
                    wasHome: data.wasHome,
                    totalPoints: data.totalPoints,
                    minutes: data.minutes,
                    goals: data.goals,
                    assists: data.assists,
                    conceded: data.conceded,
                    xG: data.xG,
                    xA: data.xA,
                    xC: data.xC,
                    saves: data.saves,
                    bonus: data.bonus,
                    bps: data.bps,
                })),
            );
        }
        return gameweekMap;
    }

    async update(playerGameStats: PlayerGameStats): Promise<void> {
        const {
            playerId,
            fixture,
            gameweek,
            wasHome,
            totalPoints,
            minutes,
            goals,
            assists,
            conceded,
            xG,
            xA,
            xC,
            saves,
            bonus,
            bps,
        } = PlayerGameStatsSchema.parse(playerGameStats);
        await PlayerFixtureStatsEntity.upsert({
            playerId: playerId,
            fixture: fixture,
            gameweek: gameweek,
            wasHome: wasHome,
            totalPoints: totalPoints,
            minutes: minutes,
            goals: goals,
            assists: assists,
            conceded: conceded,
            xG: xG,
            xA: xA,
            xC: xC,
            saves: saves,
            bonus: bonus,
            bps: bps,
        }).go();
    }
}
