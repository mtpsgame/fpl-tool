import { Entity } from 'electrodb';
import { Status, StatusSchema } from '../models/status.model';
import { IStatusRepository } from '../ports/repositories/status.repository.port';
import { Configuration } from './fpl-dynamo';

const StatusEntity = new Entity(
    {
        model: {
            entity: 'status',
            version: '1',
            service: 'fpl',
        },
        attributes: {
            timestamp: {
                type: 'string',
                required: true,
            },
            currentGameweek: {
                type: 'string',
                required: true,
            },
        },
        indexes: {
            primary: {
                pk: {
                    field: 'pk',
                    composite: [],
                    template: 'status',
                },
                sk: {
                    field: 'sk',
                    composite: [],
                    template: 'status',
                },
            },
        },
    },
    Configuration,
);

export class StatusRepository implements IStatusRepository {
    async get(): Promise<Status | undefined> {
        const status = await StatusEntity.get({}).go();
        if (!status.data) {
            return undefined;
        }
        return {
            lastUpdated: status.data.timestamp,
            currentGameweek: status.data.currentGameweek,
        };
    }

    async update(status: Status): Promise<void> {
        const { lastUpdated, currentGameweek } = StatusSchema.parse(status);
        await StatusEntity.upsert({
            timestamp: lastUpdated,
            currentGameweek: currentGameweek,
        }).go();
    }
}
