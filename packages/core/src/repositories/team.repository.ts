import { Entity } from 'electrodb';
import { Team, TeamSchema } from '../models/team.model';
import { ITeamRepository } from '../ports/repositories/team.repository.port';
import { Configuration } from './fpl-dynamo';

const TeamEntity = new Entity(
    {
        model: {
            entity: 'team',
            version: '1',
            service: 'fpl',
        },
        attributes: {
            id: {
                type: 'string',
                required: true,
            },
            shortName: {
                type: 'string',
                required: true,
            },
            name: {
                type: 'string',
                required: true,
            },
        },
        indexes: {
            primary: {
                pk: {
                    field: 'pk',
                    composite: [],
                    template: 'team',
                },
                sk: {
                    field: 'sk',
                    composite: ['id'],
                    cast: 'string',
                    template: '${id}',
                },
            },
        },
    },
    Configuration,
);

export class TeamRepository implements ITeamRepository {
    async getAll(): Promise<Team[]> {
        const status = await TeamEntity.query.primary({}).go();
        return status.data.map((team) => ({
            id: team.id,
            shortName: team.shortName,
            name: team.name,
        }));
    }

    async update(team: Team): Promise<void> {
        const { id, shortName, name } = TeamSchema.parse(team);
        await TeamEntity.upsert({
            id: id,
            shortName: shortName,
            name: name,
        }).go();
    }
}
