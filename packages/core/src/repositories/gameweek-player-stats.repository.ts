import { Entity } from 'electrodb';
import { GameweekIdSchema } from '../models/fixture.model';
import {
    GameweekPlayerStats,
    GameweekPlayerStatsSchema,
} from '../models/gameweek-player-stats.model';
import { IGameweekPlayerStatsRepository } from '../ports/repositories/gameweek-player-stats.repository.port';
import { Configuration } from './fpl-dynamo';

const GameweekPlayerStatsEntity = new Entity(
    {
        model: {
            entity: 'gameweek-player-stats',
            version: '1',
            service: 'fpl',
        },
        attributes: {
            gameweek: {
                type: 'string',
                required: true,
            },
            stats: {
                type: 'list',
                required: true,
                items: {
                    type: 'map',
                    properties: {
                        playerId: {
                            type: 'string',
                            required: true,
                        },
                        playerStats: {
                            type: 'list',
                            required: true,
                            items: {
                                type: 'map',
                                required: true,
                                properties: {
                                    playerId: {
                                        type: 'string',
                                        required: true,
                                    },
                                    fixture: {
                                        type: 'string',
                                        required: true,
                                    },
                                    gameweek: {
                                        type: 'string',
                                        required: true,
                                    },
                                    wasHome: {
                                        type: 'boolean',
                                        required: true,
                                    },
                                    totalPoints: {
                                        type: 'number',
                                        required: true,
                                    },
                                    minutes: {
                                        type: 'number',
                                        required: true,
                                    },
                                    goals: {
                                        type: 'number',
                                        required: true,
                                    },
                                    assists: {
                                        type: 'number',
                                        required: true,
                                    },
                                    conceded: {
                                        type: 'number',
                                        required: true,
                                    },
                                    xG: {
                                        type: 'number',
                                        required: true,
                                    },
                                    xA: {
                                        type: 'number',
                                        required: true,
                                    },
                                    xC: {
                                        type: 'number',
                                        required: true,
                                    },
                                    saves: {
                                        type: 'number',
                                        required: true,
                                    },
                                    bonus: {
                                        type: 'number',
                                        required: true,
                                    },
                                    bps: {
                                        type: 'number',
                                        required: true,
                                    },
                                },
                            },
                        },
                    },
                },
            },
        },
        indexes: {
            primary: {
                pk: {
                    field: 'pk',
                    composite: [],
                    template: 'gameweek-stats-consolidation',
                },
                sk: {
                    field: 'sk',
                    composite: ['gameweek'],
                    template: 'gameweek#${gameweek}',
                },
            },
        },
    },
    Configuration,
);

export class GameweekPlayerStatsRepository implements IGameweekPlayerStatsRepository {
    async getGameweekStats(
        fromGameweek: number,
        toGameweek: number,
    ): Promise<GameweekPlayerStats[]> {
        const gameweeksStats = [];
        const result = await GameweekPlayerStatsEntity.query
            .primary({})
            .between(
                { gameweek: GameweekIdSchema.parse(fromGameweek) },
                { gameweek: GameweekIdSchema.parse(toGameweek) },
            )
            .go();
        gameweeksStats.push(...result.data);

        return result.data.map((gameweek) => ({
            gameweek: gameweek.gameweek,
            stats: gameweek.stats.map((playerStat) => ({
                playerId: playerStat.playerId,
                playerStats: playerStat.playerStats.map((playerStats) => ({
                    playerId: playerStats.playerId,
                    fixture: playerStats.fixture,
                    gameweek: playerStats.gameweek,
                    wasHome: playerStats.wasHome,
                    totalPoints: playerStats.totalPoints,
                    minutes: playerStats.minutes,
                    goals: playerStats.goals,
                    assists: playerStats.assists,
                    conceded: playerStats.conceded,
                    xG: playerStats.xG,
                    xA: playerStats.xA,
                    xC: playerStats.xC,
                    saves: playerStats.saves,
                    bonus: playerStats.bonus,
                    bps: playerStats.bps,
                })),
            })),
        }));
    }

    async update(gameweekStats: GameweekPlayerStats): Promise<void> {
        const { gameweek, stats } = GameweekPlayerStatsSchema.parse(gameweekStats);
        await GameweekPlayerStatsEntity.upsert({
            gameweek: gameweek,
            stats: stats.map(({ playerId, playerStats }) => ({
                playerId: playerId,
                playerStats: playerStats.map((playerStats) => ({
                    playerId: playerStats.playerId,
                    fixture: playerStats.fixture,
                    gameweek: playerStats.gameweek,
                    wasHome: playerStats.wasHome,
                    totalPoints: playerStats.totalPoints,
                    minutes: playerStats.minutes,
                    goals: playerStats.goals,
                    assists: playerStats.assists,
                    conceded: playerStats.conceded,
                    xG: playerStats.xG,
                    xA: playerStats.xA,
                    xC: playerStats.xC,
                    saves: playerStats.saves,
                    bonus: playerStats.bonus,
                    bps: playerStats.bps,
                })),
            })),
        }).go();
    }
}
