import { DynamoDBClient } from '@aws-sdk/client-dynamodb';
import { EntityConfiguration } from 'electrodb';
import { Table } from 'sst/node/table';

const client = new DynamoDBClient({});

export const Configuration: EntityConfiguration = {
    table: Table['fpl-table'].tableName,
    client: client,
};
