import { Entity } from 'electrodb';
import { Player, PlayerId, PlayerIdSchema, PlayerSchema } from '../models/player.model';
import { IPlayerRepository } from '../ports/repositories/player.repository.port';
import { Configuration } from './fpl-dynamo';

const PlayerEntity = new Entity(
    {
        model: {
            entity: 'player',
            version: '1',
            service: 'fpl',
        },
        attributes: {
            id: {
                type: 'string',
                required: true,
            },
            name: {
                type: 'string',
                required: true,
            },
            teamId: {
                type: 'string',
                required: true,
            },
            position: {
                type: 'number',
                required: true,
            },
            cost: {
                type: 'number',
                required: true,
            },
            selectedByPercent: {
                type: 'number',
                required: true,
            },
            status: {
                type: 'string',
                required: true,
            },
        },
        indexes: {
            primary: {
                pk: {
                    field: 'pk',
                    composite: [],
                    template: 'player',
                },
                sk: {
                    field: 'sk',
                    composite: ['id'],
                    template: '${id}',
                },
            },
        },
    },
    Configuration,
);

export class PlayerRepository implements IPlayerRepository {
    async get(id: number | PlayerId): Promise<Player | undefined> {
        const player = await PlayerEntity.get({ id: PlayerIdSchema.parse(id) }).go();
        if (!player.data) {
            return undefined;
        }
        return {
            id: player.data.id,
            name: player.data.name,
            teamId: player.data.teamId,
            position: player.data.position,
            cost: player.data.cost,
            selectedByPercent: player.data.selectedByPercent,
            status: player.data.status,
        };
    }

    async getMany(ids: PlayerId[]): Promise<Player[]> {
        // Have to do the query in batches otherwise the query size exceeds the limit
        const batchSize = 100;
        const groups = [];
        for (let i = 0; i < ids.length; i += batchSize) {
            const group = ids.slice(i, i + batchSize);
            groups.push(group);
        }

        const players = await Promise.all(
            groups.map(async (group) => {
                const result = await PlayerEntity.query
                    .primary({})
                    .where((p, op) => `${group.map((id) => op.eq(p.id, id)).join(' OR ')}`)
                    .go();
                return result.data;
            }),
        );

        const flattenedPlayers = players.flat();

        return flattenedPlayers.map((player) => ({
            id: player.id,
            name: player.name,
            teamId: player.teamId,
            position: player.position,
            cost: player.cost,
            selectedByPercent: player.selectedByPercent,
            status: player.status,
        }));
    }

    async update(player: Player): Promise<void> {
        const { id, name, teamId, position, cost, selectedByPercent, status } =
            PlayerSchema.parse(player);
        await PlayerEntity.upsert({
            id: id,
            name: name,
            teamId: teamId,
            position: position,
            cost: cost,
            selectedByPercent: selectedByPercent,
            status: status,
        }).go();
    }
}
