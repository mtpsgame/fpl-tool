import { Team } from '../../models/team.model';

export interface ITeamRepository {
    getAll(): Promise<Team[]>;
    update(team: Team): Promise<void>;
}
