import { Fixture } from '../../models/fixture.model';

export interface IFixtureRepository {
    getAll(): Promise<Fixture[]>;
    update(fixture: Fixture): Promise<void>;
}
