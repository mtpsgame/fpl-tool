import { GameweekId } from '../../models/fixture.model';
import { PlayerGameStats } from '../../models/player-game-stats.model';

export interface IPlayerGameStatsRepository {
    getAllInGameweekRange(
        fromGameweek: number,
        toGameweek: number,
    ): Promise<Map<GameweekId, PlayerGameStats[]>>;
    update(PlayerGameStats: PlayerGameStats): Promise<void>;
}
