import { GameweekPlayerStats } from '../../models/gameweek-player-stats.model';

export interface IGameweekPlayerStatsRepository {
    getGameweekStats(fromGameweek: number, toGameweek: number): Promise<GameweekPlayerStats[]>;
    update(gameweekStats: GameweekPlayerStats): Promise<void>;
}
