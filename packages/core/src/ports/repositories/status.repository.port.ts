import { Status } from '../../models/status.model';

export interface IStatusRepository {
    get(): Promise<Status | undefined>;
    update(status: Status): Promise<void>;
}
