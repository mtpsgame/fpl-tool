import { Player, PlayerId } from '../../models/player.model';

export interface IPlayerRepository {
    get(id: number | PlayerId): Promise<Player | undefined>;
    getMany(ids: PlayerId[]): Promise<Player[]>;
    update(player: Player): Promise<void>;
}
