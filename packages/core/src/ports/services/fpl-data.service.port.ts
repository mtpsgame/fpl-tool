import { Fixture } from '../../models/fixture.model';
import { PlayerGameStats } from '../../models/player-game-stats.model';
import { Player } from '../../models/player.model';
import { Team } from '../../models/team.model';

export type Bootstrap = {
    teams: Team[];
    players: Player[];
};

export interface IFplDataService {
    getBootstrapData(): Promise<Bootstrap>;
    getFixtures(): Promise<Fixture[]>;
    getPlayerHistory(playerId: number): Promise<PlayerGameStats[]>;
}
