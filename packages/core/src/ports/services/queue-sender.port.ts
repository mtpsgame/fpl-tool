import { QueueResources } from 'sst/node/queue';

export interface IQueueSender {
    send(queueName: keyof QueueResources, message: string): Promise<void>;
}
