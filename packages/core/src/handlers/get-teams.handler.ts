import { inject, injectable } from 'tsyringe';
import { Team } from '../models/team.model';
import { ITeamRepository } from '../ports/repositories/team.repository.port';

@injectable()
export class GetTeamsHandler {
    constructor(
        @inject('ITeamRepository')
        private teamRepository: ITeamRepository,
    ) {}

    async handle(): Promise<Team[]> {
        return await this.teamRepository.getAll();
    }
}
