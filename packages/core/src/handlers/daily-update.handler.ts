import { inject, injectable } from 'tsyringe';
import { IFixtureRepository } from '../ports/repositories/fixture.repository.port';
import { IPlayerRepository } from '../ports/repositories/player.repository.port';
import { ITeamRepository } from '../ports/repositories/team.repository.port';
import { IFplDataService } from '../ports/services/fpl-data.service.port';
import { IPlayerGameStatsRepository } from '../ports/repositories/player-game-stats.repository.port';
import { GameweekId, GameweekIdSchema } from '../models/fixture.model';
import { GameweekPlayerStats } from '../models/gameweek-player-stats.model';
import { PlayerGameStats } from '../models/player-game-stats.model';
import { PlayerId } from '../models/player.model';
import { IGameweekPlayerStatsRepository } from '../ports/repositories/gameweek-player-stats.repository.port';
import { IStatusRepository } from '../ports/repositories/status.repository.port';

@injectable()
export class DailyUpdateHandler {
    constructor(
        @inject('IFplDataService')
        private readonly fplDataService: IFplDataService,
        @inject('ITeamRepository')
        private readonly teamRepository: ITeamRepository,
        @inject('IPlayerRepository')
        private readonly playerRepository: IPlayerRepository,
        @inject('IFixtureRepository')
        private readonly fixtureRepository: IFixtureRepository,
        @inject('IPlayerGameStatsRepository')
        private readonly playerGameStatsRepository: IPlayerGameStatsRepository,
        @inject('IGameweekPlayerStatsRepository')
        private readonly gameweekPlayerStatsRepository: IGameweekPlayerStatsRepository,
        @inject('IStatusRepository')
        private readonly statusRepository: IStatusRepository,
    ) {}

    async handle(): Promise<void> {
        const { playerIds } = await this.updateBootstrap();
        await this.updatePlayerStats(playerIds);
        const { currentGameweek } = await this.consolidatePlayerStats();
        await this.updateStatus(currentGameweek);
    }

    private async updateBootstrap(): Promise<{ playerIds: string[] }> {
        const { teams, players } = await this.fplDataService.getBootstrapData();
        for (const team of teams) {
            await this.teamRepository.update(team);
        }
        console.log('Updated teams');

        for (const player of players) {
            await this.playerRepository.update(player);
        }
        console.log('Updated players');

        const fixtures = await this.fplDataService.getFixtures();
        for (const fixture of fixtures) {
            await this.fixtureRepository.update(fixture);
        }
        console.log('Updated fixtures');

        return {
            playerIds: players.map((p) => p.id),
        };
    }

    private async updatePlayerStats(playerIds: string[]): Promise<void> {
        for (const playerId of playerIds) {
            const history = await this.fplDataService.getPlayerHistory(Number(playerId));
            for (const playerFixtureStats of history) {
                await this.playerGameStatsRepository.update(playerFixtureStats);
            }
            console.log(`Updated player stats ${playerId}`);
        }
    }

    private async consolidatePlayerStats(): Promise<{ currentGameweek: number }> {
        const result = await this.playerGameStatsRepository.getAllInGameweekRange(1, 38);

        const gameweekStats = new Map<GameweekId, Map<PlayerId, PlayerGameStats[]>>();
        [...result.keys()].forEach((gameweek) => {
            const statsForGameweek = result.get(gameweek)!;
            const statsForGameweekMap = new Map<string, PlayerGameStats[]>();
            for (const playerStats of statsForGameweek) {
                if (!statsForGameweekMap.has(playerStats.playerId)) {
                    statsForGameweekMap.set(playerStats.playerId, [playerStats]);
                } else {
                    statsForGameweekMap.get(playerStats.playerId)!.push(playerStats);
                }
            }
            gameweekStats.set(gameweek, statsForGameweekMap);
        });

        for (const [gameweek, weekStats] of gameweekStats) {
            const gameweekPlayerStats: GameweekPlayerStats = {
                gameweek: gameweek,
                stats: [...weekStats.keys()].map((playerId) => ({
                    playerId,
                    playerStats: weekStats.get(playerId)!,
                })),
            };

            await this.gameweekPlayerStatsRepository.update(gameweekPlayerStats);
            console.log(`Consolidated gameweek stats ${gameweek}`);
        }

        return {
            currentGameweek: Math.max(gameweekStats.size, 1),
        };
    }

    private async updateStatus(currentGameweek: number): Promise<void> {
        await this.statusRepository.update({
            lastUpdated: new Date(Date.now()).toISOString(),
            currentGameweek: GameweekIdSchema.parse(currentGameweek),
        });
        console.log('Updated status');
    }
}
