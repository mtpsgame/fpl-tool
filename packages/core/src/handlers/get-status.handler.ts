import { inject, injectable } from 'tsyringe';
import { Status } from '../models/status.model';
import { IStatusRepository } from '../ports/repositories/status.repository.port';

@injectable()
export class GetStatusHandler {
    constructor(
        @inject('IStatusRepository')
        private statusRepository: IStatusRepository,
    ) {}

    async handle(): Promise<Status | undefined> {
        const status = await this.statusRepository.get();
        return status;
    }
}
