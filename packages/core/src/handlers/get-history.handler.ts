import { inject, injectable } from 'tsyringe';
import { PlayerGameStats } from '../models/player-game-stats.model';
import { PlayerHistory, PlayerHistorySchema } from '../models/player-history.model';
import { PlayerId } from '../models/player.model';
import { IGameweekPlayerStatsRepository } from '../ports/repositories/gameweek-player-stats.repository.port';
import { IPlayerRepository } from '../ports/repositories/player.repository.port';

@injectable()
export class GetHistoryHandler {
    constructor(
        @inject('IGameweekPlayerStatsRepository')
        private gameweekPlayerStatsRepository: IGameweekPlayerStatsRepository,
        @inject('IPlayerRepository')
        private playerRepository: IPlayerRepository,
    ) {}

    async handle(
        fromGameweek: number,
        toGameweek: number,
        minimumAvgMinsPer90: number,
    ): Promise<PlayerHistory[]> {
        const gameweeks = await this.gameweekPlayerStatsRepository.getGameweekStats(
            fromGameweek,
            toGameweek,
        );

        const playerStats = new Map<PlayerId, PlayerGameStats[]>();
        for (const gameweek of gameweeks) {
            for (const stat of gameweek.stats) {
                if (playerStats.has(stat.playerId)) {
                    playerStats.get(stat.playerId)!.push(...stat.playerStats);
                } else {
                    playerStats.set(stat.playerId, [...stat.playerStats]);
                }
            }
        }

        const filteredPlayerStats = new Map<PlayerId, PlayerGameStats[]>();
        for (const [playerId, stats] of playerStats) {
            if (
                stats.map((stat) => stat.minutes).reduce((a, b) => a + b, 0) / stats.length >=
                minimumAvgMinsPer90
            ) {
                filteredPlayerStats.set(playerId, stats);
            }
        }

        const players = await this.playerRepository.getMany(Array.from(filteredPlayerStats.keys()));

        const history: PlayerHistory[] = [];
        for (const [playerId, stats] of filteredPlayerStats) {
            const player = players.find((p) => p.id === playerId);
            if (player) {
                history.push(
                    PlayerHistorySchema.parse({
                        ...player,
                        stats,
                    }),
                );
            }
        }

        return history;
    }
}
