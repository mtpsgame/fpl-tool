import 'reflect-metadata';
import { instance, mock, when } from 'ts-mockito';
import { beforeEach, describe, expect, it } from 'vitest';
import { GetStatusHandler } from '../src/handlers/get-status.handler';
import { Status } from '../src/models/status.model';
import { IStatusRepository } from '../src/ports/repositories/status.repository.port';

describe('GetStatusHandler', () => {
    let mockStatusRepository: IStatusRepository;
    let sut: GetStatusHandler;

    beforeEach(() => {
        mockStatusRepository = mock<IStatusRepository>();
        sut = new GetStatusHandler(instance(mockStatusRepository));
    });

    it('Should return status', async () => {
        // Given
        const status: Status = {
            lastUpdated: '2024-01-01',
            currentGameweek: 10,
        };
        when(mockStatusRepository.get()).thenResolve(status);

        // When
        const result = await sut.handle();

        // Then
        expect(result).toEqual(status);
    });

    it('Should throw error when status is not found', async () => {
        // Given
        when(mockStatusRepository.get()).thenThrow();

        // When
        const result = sut.handle();

        // Then
        await expect(result).rejects.toThrow('No status found');
    });
});
