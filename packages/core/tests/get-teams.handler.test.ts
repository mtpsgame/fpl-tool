import 'reflect-metadata';
import { instance, mock, when } from 'ts-mockito';
import { beforeEach, describe, expect, it } from 'vitest';
import { GetTeamsHandler } from '../src/handlers/get-teams.handler';
import { Team } from '../src/models/team.model';
import { ITeamRepository } from '../src/ports/repositories/team.repository.port';

describe('GetTeamsHandler', () => {
    let mockTeamRepository: ITeamRepository;
    let sut: GetTeamsHandler;

    beforeEach(() => {
        mockTeamRepository = mock<ITeamRepository>();
        sut = new GetTeamsHandler(instance(mockTeamRepository));
    });

    it('Should return teams', async () => {
        // Given
        const teams: Team[] = [
            { id: 1, name: 'Team 1', shortName: 'TM1' },
            { id: 2, name: 'Team 2', shortName: 'TM2' },
        ];
        when(mockTeamRepository.getAll()).thenResolve(teams);

        // When
        const result = await sut.handle();

        // Then
        expect(result).toEqual(teams);
    });
});
