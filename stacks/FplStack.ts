import { Duration, Fn } from 'aws-cdk-lib';
import { Certificate } from 'aws-cdk-lib/aws-certificatemanager';
import { AllowedMethods, ViewerProtocolPolicy } from 'aws-cdk-lib/aws-cloudfront';
import { HttpOrigin } from 'aws-cdk-lib/aws-cloudfront-origins';
import {
    Api,
    ApiDomainProps,
    ApiRouteProps,
    Cron,
    Queue,
    StackContext,
    StaticSite,
    StaticSiteDomainProps,
    Table,
} from 'sst/constructs';
import { BindingResource } from 'sst/constructs/util/binding';

export function API({ stack }: StackContext) {
    stack.setDefaultFunctionProps({
        runtime: 'nodejs18.x',
        memorySize: '512 MB',
        architecture: 'arm_64',
    });

    const table = new Table(stack, 'fpl-table', {
        fields: {
            pk: 'string',
            sk: 'string',
            gsi1pk: 'string',
            gsi1sk: 'string',
        },
        globalIndexes: {
            gsi1: {
                partitionKey: 'gsi1pk',
                sortKey: 'gsi1sk',
            },
        },
        primaryIndex: {
            partitionKey: 'pk',
            sortKey: 'sk',
        },
    });

    const updateQueue = new Queue(stack, 'daily-update-queue', {
        cdk: {
            queue: {
                visibilityTimeout: Duration.seconds(300),
            },
        },
        consumer: {
            function: {
                handler: buildEventName('daily-update', 'handle'),
                timeout: 300,
                bind: [table],
            },
        },
    });

    new Cron(stack, 'daily-update-cron', {
        job: {
            function: {
                handler: buildCronName('daily-update', 'handle'),
                bind: [table, updateQueue],
            },
        },
        schedule: 'cron(0 3 * * ? *)',
        enabled: stack.stage === 'production',
    });

    const customDomainGateway: ApiDomainProps | undefined =
        process.env.CUSTOM_DOMAIN_NAME && process.env.CUSTOM_DOMAIN_GATEWAY_CERT_ARN
            ? {
                  domainName: process.env.CUSTOM_DOMAIN_NAME,
                  isExternalDomain: true,
                  cdk: {
                      certificate: Certificate.fromCertificateArn(
                          stack,
                          'FplToolApiGatewayCert',
                          process.env.CUSTOM_DOMAIN_GATEWAY_CERT_ARN,
                      ),
                  },
              }
            : undefined;
    const customDomainDistribution: StaticSiteDomainProps | undefined =
        process.env.CUSTOM_DOMAIN_NAME && process.env.CUSTOM_DOMAIN_DISTRIBUTION_CERT_ARN
            ? {
                  domainName: process.env.CUSTOM_DOMAIN_NAME,
                  isExternalDomain: true,
                  cdk: {
                      certificate: Certificate.fromCertificateArn(
                          stack,
                          'FplToolDistributionCert',
                          process.env.CUSTOM_DOMAIN_DISTRIBUTION_CERT_ARN,
                      ),
                  },
              }
            : undefined;

    const api = new Api(stack, 'fpl-api', {
        defaults: {
            function: {
                memorySize: '1 GB',
                bind: [table],
            },
        },
        customDomain: customDomainGateway,
        routes: buildRoutes(stack.stage, updateQueue),
    });

    const distribution = new StaticSite(stack, 'frontend', {
        path: 'packages/frontend',
        environment: {
            VITE_API_BASE_URL: api.customDomainUrl
                ? `${api.customDomainUrl}/api`
                : `${api.url}/api`,
        },
        buildOutput: 'dist',
        buildCommand: 'pnpm run build',
        customDomain: customDomainDistribution,
        cdk: {
            distribution: {
                additionalBehaviors: customDomainDistribution
                    ? {
                          '/api/*': {
                              origin: new HttpOrigin(
                                  Fn.parseDomainName(api.cdk.httpApi.apiEndpoint),
                              ),
                              viewerProtocolPolicy: ViewerProtocolPolicy.HTTPS_ONLY,
                              allowedMethods: AllowedMethods.ALLOW_ALL,
                              cachePolicy: {
                                  cachePolicyId: '4135ea2d-6df8-44a3-9df3-4b5a84be39ad',
                              },
                          },
                      }
                    : undefined,
            },
        },
    });

    stack.addOutputs({
        CustomApiEndpoint: api.customDomainUrl,
        ApiEndpoint: api.url,
        FrontendURL: distribution.url,
    });
}

function buildRoutes(
    stage: string,
    updateQueue: BindingResource,
): Record<string, ApiRouteProps<string>> {
    const routes: Record<string, ApiRouteProps<string>> = {
        'GET /api/status': buildControllerHandlerName('fpl', 'getStatusHandler'),
        'GET /api/teams': buildControllerHandlerName('fpl', 'getTeamsHandler'),
        'GET /api/history': buildControllerHandlerName('fpl', 'getHistoryHandler'),
    };

    if (stage !== 'production') {
        routes['GET /'] = buildControllerHandlerName('root', 'handler');
        routes['POST /jobs/update'] = {
            function: {
                handler: buildControllerHandlerName('jobs', 'updateHandler'),
                bind: [updateQueue],
            },
        };
    }

    return routes;
}

function buildControllerHandlerName(file: string, method: string): string {
    return `packages/functions/src/controllers/${file}.controller.${method}`;
}

function buildEventName(file: string, method: string): string {
    return `packages/functions/src/events/${file}.handler.${method}`;
}

function buildCronName(file: string, method: string): string {
    return `packages/functions/src/cron/${file}.handler.${method}`;
}
